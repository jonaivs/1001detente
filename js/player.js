let defaulttext = 'Écouter ma présentation: -1:09 min';

function dispDuration() {
	let time = Math.round(Amplitude.getSongDuration() - Amplitude.getSongPlayedSeconds());
	//test si time est un nombre
	if (time !== time) {
		return defaulttext;
	}
	let minutes = Math.floor(time / 60);
	let seconds = time - minutes * 60;
	return 'Écouter ma présentation: -' + minutes + ':' + String(seconds).padStart(2, '0') + ' min';
}

Amplitude.init({
	"songs": [
		{
			"url": "./audio/audio.mp3",
		}
	],
	callbacks: {
		timeupdate: function () {
			document.getElementById('time').innerHTML = dispDuration();
			const div = document.querySelector('#fill');
			div.style.width = Amplitude.getSongPlayedPercentage() + '%';
			//document.querySelector('#player').style.background = 'white';
		},
		ended: function () {
			document.querySelector('#fill').style.width = '0%';
			document.getElementById('time').innerHTML = defaulttext;
		},
		stop: function () {
			document.querySelector('#fill').style.width = '0%';
			document.getElementById('time').innerHTML = defaulttext;
		},
		initialized: function () {
			document.getElementById('time').innerHTML = dispDuration();
		}
	},
	debug: false
});